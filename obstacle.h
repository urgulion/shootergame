#include <SFML/Graphics.hpp>

#ifndef OBSTACLE_H
#define OBSTACLE_H

class Obstacle
{
private:
	int x, y;
	float height, width;
	sf::RectangleShape shape;

public:
	Obstacle();
	void setParameters(int x, int y, float height, float width);

	int getX();
	int getY();
	float getWidth();
	float getHeight();
	sf::RectangleShape getShape();
};

#endif