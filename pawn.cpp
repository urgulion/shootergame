#include "pawn.h"
	
Pawn::Pawn()
{
	score = 0;
	width = 45;
	height = 45;
	
	remainBullets = maxBullets = 1;
	bullet = new Bullet[maxBullets];
};

void Pawn::positionCorrection(Obstacle *obstacle, int n)
{
	if (x < 0 + width / 2)
	{
		x = 0 + width / 2;
	}
	if (y < 140 + height / 2)
	{
		y = 140 + height / 2;
	}
	if (x > 800 - width / 2)
	{
		x = 800 - width / 2;
	}
	if (y > 640 - height / 2)
	{
		y = 640 - height / 2;
	}

};

bool Pawn::collisionDetection(Obstacle *obstacle, int n)
{
	for (int i = 0; i < n; i++)
	{
		if ((x + width / 2 > obstacle[i].getX()) && (x - width / 2 < obstacle[i].getX() + obstacle[i].getWidth())
			&& (y + height / 2 > obstacle[i].getY()) && (y - height / 2 < obstacle[i].getY() + obstacle[i].getHeight()))
		{
			return true;
		}
	}
	return false;
}

void Pawn::shoot()
{
	remainBullets = maxBullets;
	for (int i = 0; i < maxBullets; i++)
	{
		if (bullet[i].getActive())
		{
			remainBullets--;
		}
	}

	if (remainBullets > 0)
	{
		int tmp = findBullet(bullet);
		bullet[tmp].setParameters(this->x, this->y, direction);
	}
}

int Pawn::findBullet(Bullet *bullet)
{
	for (int i = 0; i < maxBullets; i++)
	{
		if (!bullet[i].getActive())
		{
			return i;
		}
	}
}

void Pawn::setX(int x)
{
	this->x = x;
};
int Pawn::getX()
{
	return x;
};

void Pawn::setY(int y)
{
	this->y = y;
};
int Pawn::getY()
{
	return y;
};

void Pawn::setScore(int score)
{
	this->score = score;
};
int Pawn::getScore()
{
	return score;
};

void Pawn::setWidth(float width)
{
	this->width = width;
};
float Pawn::getWidth()
{
	return width;
};

void Pawn::setHeight(float height)
{
	this->height = height;
};
float Pawn::getHeight()
{
	return height;
};

void Pawn::setDirection(float direction)
{
	this->direction = direction;
};
float Pawn::getDirection()
{
	return direction;
};

void Pawn::setTexture(sf::Texture texture)
{
	this->texture = texture;
};
sf::Texture Pawn::getTexture()
{
	return texture;
};

void Pawn::setSprite(sf::Sprite sprite)
{
	this->sprite = sprite;
};
sf::Sprite Pawn::getSprite()
{
	return sprite;
};

int Pawn::getMaxBullets()
{
	return maxBullets;
}
