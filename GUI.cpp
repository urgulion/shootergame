#include "GUI.h"

GUI::GUI()
{
	info.setFillColor(sf::Color(102, 153, 153));
	plane.setFillColor(sf::Color(255, 255, 255));

	if (!font.loadFromFile("fonts/arial.ttf"))
	{
		error();
	}
};

void GUI::initGame(sf::Vector2u windowSize, sf::Vector2f planeSize, sf::Vector2f infoSize, Obstacle *obstacle, int n)
{
	info.setSize(infoSize);
	plane.setSize(planeSize);
	plane.setPosition(0, infoSize.y);

	this->windowSize = windowSize;
	this->n = n;
	this->obstacle = obstacle;
};

void GUI::error()
{
	setlocale(LC_ALL, "rus");
	std::cout << "�� ����� ������� ��������� ���� ���������� ������."<<std::endl;
	system("pause");
}

std::string GUI::updateScore()
{
	std::string playerScore;
	std::string enemyScore;
	std::string scoreString; 
	
	playerScore = std::to_string(player.getScore());
	enemyScore = std::to_string(enemy.getScore());
	scoreString = playerScore + ":" + enemyScore;
	return scoreString;
}

void GUI::tick()
{
	sf::RenderWindow window(sf::VideoMode(windowSize.x, windowSize.y), "1X1");

	sf::Clock clock;
	sf::Text score;
	score.setFont(font);
	score.setCharacterSize(100);
	score.setFillColor(sf::Color::Black);
	score.setPosition(info.getSize().x / 2.5, info.getSize().y / 4.5);
	score.setString(updateScore());
	

	while (window.isOpen())
	{
		float deltaTime;
		deltaTime = clock.getElapsedTime().asMicroseconds();
		clock.restart();
		deltaTime = deltaTime / 60;
		
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}

		player.move(deltaTime, obstacle, n);
		enemy.update(deltaTime, obstacle, n, sf::Vector2f(player.getX(), player.getY()), player.getDirection());

		score.setString(updateScore());
		
		window.clear();
		window.draw(info);
		window.draw(plane);

		for (int i = 0; i < n; i++)
		{
			window.draw(obstacle[i].getShape());
		}

		for (int i = 0; i < player.getMaxBullets(); i++)
		{
			if (player.bullet[i].getActive() == true)
			{
				if (player.bullet[i].move(deltaTime, obstacle, n, enemy.getX(), enemy.getY(), enemy.getHeight(), enemy.getWidth()))
				{
					player.setScore(player.getScore() + 1);
				}
				window.draw(player.bullet[i].getSprite());
			}
		}

		for (int i = 0; i < enemy.getMaxBullets(); i++)
		{
			if (enemy.bullet[i].getActive() == true)
			{
				if (enemy.bullet[i].move(deltaTime, obstacle, n, player.getX(), player.getY(), player.getHeight(), player.getWidth()))
				{
					enemy.setScore(enemy.getScore() + 1);
				}
				window.draw(enemy.bullet[i].getSprite());
			}
		}

		window.draw(player.getSprite());
		window.draw(enemy.getSprite());
		window.draw(score);
		window.display();
	}
};
