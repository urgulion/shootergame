#include "enemy.h"

Enemy::Enemy() : Pawn()
{
	x = 750;
	y = 200;
	direction = rand() % 180;

	target = false;

	searchRadiusPlayer = 200;
	searchRadiusBullet = 20;

	image.loadFromFile("images/enemy.png");
	image.createMaskFromColor(sf::Color(255, 255, 255));
	texture.loadFromImage(image);
	sprite.setTexture(texture);
	sprite.setPosition(x, y);
	sprite.setOrigin(width / 2, height / 2);
	sprite.setRotation(direction);
}

void Enemy::update(float deltaTime, Obstacle *obstacle, int n, sf::Vector2f playerPosition, float playerDirection)
{
	float tmp;
	tmp = (playerPosition.x - x) * (playerPosition.x - x) + (playerPosition.y - y) * (playerPosition.y - y);
	if (tmp <= searchRadiusPlayer * searchRadiusPlayer) //��������, �������� �� ����� � ������ ������ ��
	{
		target = true;
		calculate(deltaTime, obstacle, n, playerPosition, playerDirection);
	}
	else
	{
		target = false;
		move(deltaTime, obstacle, n, playerPosition);
	}	
}

void Enemy::rotateToPlayer(sf::Vector2f playerPosition)
{
	float dX = playerPosition.x - x; //dX, dY - ������, ����������� ������, ���������� ������ ������ � ��
	float dY = playerPosition.y - y;
	direction = (atan2(dY, dX)) * 180 / 3.14159265 + 90; //������� �������� � �������
	sprite.setRotation(direction);
}

void Enemy::move(float deltaTime, Obstacle *obstacle, int n, sf::Vector2f playerPosition)
{
	/*if (target)
	{
		direction += 90;
		sprite.setRotation(direction);
	}*/

	if (!collisionDetection(obstacle, n) &&
		!((x + width / 2 > playerPosition.x - searchRadiusPlayer / 2) && (x - width / 2 < playerPosition.x + width / 2 + searchRadiusPlayer / 2)
		&& (y + height / 2 > playerPosition.y - searchRadiusPlayer / 2) && (y - height / 2 < playerPosition.y + height / 2 + searchRadiusPlayer / 2)) //�� ��������� ��������� ������� ������
		)
	{
		x += std::sin(direction * 3.14159265 / 180) * deltaTime * 0.005;
		y -= std::cos(direction * 3.14159265 / 180) * deltaTime * 0.005;
	}
	else
	{
		x -= std::sin(direction * 3.14159265 / 180) * deltaTime * 0.005;
		y += std::cos(direction * 3.14159265 / 180) * deltaTime * 0.005;
		direction--; //��� ��������� �����������
		sprite.setRotation(direction);
	}

	positionCorrection(obstacle, n);
	
	sprite.setPosition(x, y);
}

void Enemy::calculate(float deltaTime, Obstacle *obstacle, int n, sf::Vector2f playerPosition, float playerDirection)
{
	rotateToPlayer(playerPosition);
	shoot();
	move(deltaTime, obstacle, n, playerPosition);
}

void Enemy::positionCorrection(Obstacle *obstacle, int n)
{
	if (x < 0 + width / 2)
	{
		x = 0 + width / 2;
		direction = rand() % 180; 
		sprite.setRotation(direction);
	}
	if (y < 140 + height / 2)
	{
		y = 140 + height / 2;
		direction = 90 + rand() % 270;
		sprite.setRotation(direction);
	}
	if (x > 800 - width / 2)
	{
		x = 800 - width / 2;
		direction = 180 + rand() % 360;
		sprite.setRotation(direction);
	}
	if (y > 640 - height / 2)
	{
		y = 640 - height / 2;
		direction = -90 + rand() % 90;
		sprite.setRotation(direction);
	}
};