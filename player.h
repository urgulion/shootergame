#include "pawn.h"


#ifndef PLAYER_H
#define PLAYER_H

class Player : public Pawn
{
	public:
		Player();

		void move(float time, Obstacle *obstacle, int n);
};

#endif
