#include "pawn.h"

#ifndef ENEMY_H
#define ENEMY_H

class Enemy : public Pawn
{
	private:
		bool target;
		float searchRadiusPlayer;
		float searchRadiusBullet;

	public:
		Enemy();

		void update(float deltaTime, Obstacle *obstacle, int n, sf::Vector2f playerPosition, float playerDirection);
		void move(float deltaTime, Obstacle *obstacle, int n, sf::Vector2f playerPosition);
		void calculate(float deltaTime, Obstacle *obstacle, int n, sf::Vector2f playerPosition, float playerDirection);
		void rotateToPlayer(sf::Vector2f playerPosition);
		void positionCorrection(Obstacle *obstacle, int n);
};

#endif
