#include <SFML/Graphics.hpp>
#include "obstacle.h"
#include "bullet.h"

#ifndef PAWN_H
#define PAWN_H

class Pawn
{
	protected:
		double x, y;
		int score;
		float width, height, direction;
		
		int maxBullets;
		int remainBullets;

		sf::Texture texture;
		sf::Sprite sprite;
		sf::Image image;
	
	public:
		Pawn();
		Bullet *bullet;
		void positionCorrection(Obstacle *obstacle, int n);
		bool collisionDetection(Obstacle *obstacle, int n);
		int findBullet(Bullet *bullet);
		void shoot();

		void setX(int x);
		int getX();

		void setY(int y);
		int getY();

		void setScore(int score);
		int getScore();

		void setWidth(float width);
		float getWidth();

		void setHeight(float height);
		float getHeight();

		void setDirection(float direction);
		float getDirection();

		void setTexture(sf::Texture texture);
		sf::Texture getTexture();

		void setSprite(sf::Sprite sprite);
		sf::Sprite getSprite();

		int getMaxBullets();
};

#endif