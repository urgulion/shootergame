#include <SFML/Graphics.hpp>
#include <iostream>
#include "obstacle.h"
#include "player.h"
#include "enemy.h"

#ifndef GUI_H
#define GUI_H

class GUI
{
	private:
		Obstacle *obstacle;
		int n;

		sf::RectangleShape info;
		sf::RectangleShape plane;
		sf::Vector2u windowSize;
		
		Player player;
		Enemy enemy;
		
		sf::Font font;

	public:
		GUI();

		//void spriteManager();
		void initGame(sf::Vector2u windowSize, sf::Vector2f planeSize, sf::Vector2f infoSize, Obstacle *obstacle, int n);
		void tick();
		void error();
		std::string updateScore();
};

#endif
