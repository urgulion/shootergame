#include <SFML/Graphics.hpp>
#include <fstream>

#include "GUI.h"
#include "obstacle.h"


int main()
{
	sf::Vector2u windowSize(800, 640);
	sf::Vector2f planeSize(800, 500), infoSize(800, 140);
	GUI gui;

	int n = 0;
	std::ifstream fin("obstacle_parameters.txt");
	if (!fin.is_open())
	{
		gui.error();
	}
	else
	{
		fin >> n;
		Obstacle *obstacle = new Obstacle[n];
		int ox, oy, ow, oh;
		for (int i = 0; i < n; i++)
		{
			fin >> ox;
			fin >> oy;
			fin >> ow;
			fin >> oh;
			obstacle[i].setParameters(ox, oy, oh, ow);
		}
		gui.initGame(windowSize, planeSize, infoSize, obstacle, n);
		fin.close();
	}
	
	gui.tick();

	return 0;
}