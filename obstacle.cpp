#include "obstacle.h"

Obstacle::Obstacle()
{
	shape.setFillColor(sf::Color(0, 51, 0));
};

void Obstacle::setParameters(int x, int y, float height, float width)
{
	this->x = x;
	this->y = y;
	this->height = height;
	this->width = width;

	shape.setSize(sf::Vector2f(width, height));
	shape.setPosition(sf::Vector2f(x, y));
};

int Obstacle::getX()
{
	return x;
};
int Obstacle::getY()
{
	return y;
};
float Obstacle::getWidth()
{
	return width;
};
float Obstacle::getHeight()
{
	return height;
};
sf::RectangleShape Obstacle::getShape()
{
	return shape;
};