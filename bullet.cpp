#include "bullet.h"

Bullet::Bullet()
{
	isActive = false;

	width = 23;
	height = 21;

	image.loadFromFile("images/bullet.png");
	image.createMaskFromColor(sf::Color(255, 255, 255));
	texture.loadFromImage(image);
	sprite.setTexture(texture);
	sprite.setOrigin(width / 2, height / 2);
};

void Bullet::setParameters(int x, int y, float direction)
{
	isActive = true;

	this->x = x;
	this->y = y;
	this->direction = direction;

	sprite.setPosition(x, y);
};

bool Bullet::move(float deltaTime, Obstacle *obstacle, int n, double pawnX, double pawnY, float pawnHeight, float pawnWidth)
{
	x += std::sin(direction * 3.14159265 / 180) * deltaTime * 0.01;
	y -= std::cos(direction * 3.14159265 / 180) * deltaTime * 0.01;

	if ((x <= 0 + width / 2) || (y <= 140 + height / 2) || (x >= 800 - width / 2) || (y >= 640 - height / 2))
	{
		isActive = false;
	}
	else
	{
		for (int i = 0; i < n; i++)
		{
			if ((x + width / 2 > obstacle[i].getX()) && (x - width / 2 < obstacle[i].getX() + obstacle[i].getWidth())
				&& (y + height / 2 > obstacle[i].getY()) && (y - height / 2 < obstacle[i].getY() + obstacle[i].getHeight()))
			{
				direction += direction;
			}
		}
	}
	
	sprite.setPosition(x, y);

	if ((x + width / 2 > pawnX - pawnWidth/2) && (x - width / 2 < pawnX + pawnWidth/2)
		&& (y + height / 2 > pawnY - pawnHeight/2) && (y - height / 2 < pawnY + pawnHeight/2))
	{
		isActive = false;
		return true;
	}
	
	return false;
};

void Bullet::setX(int x)
{
	this->x = x;
};
int Bullet::getX()
{
	return x;
};

void Bullet::setY(int y)
{
	this->y = y;
};
int Bullet::getY()
{
	return y;
};

void Bullet::setWidth(float width)
{
	this->width = width;
};
float Bullet::getWidth()
{
	return width;
};

void Bullet::setHeight(float height)
{
	this->height = height;
};
float Bullet::getHeight()
{
	return height;
};

void Bullet::setDirection(float direction)
{
	this->direction = direction;
};
float Bullet::getDirection()
{
	return direction;
};

void Bullet::setTexture(sf::Texture texture)
{
	this->texture = texture;
};
sf::Texture Bullet::getTexture()
{
	return texture;
};

void Bullet::setSprite(sf::Sprite sprite)
{
	this->sprite = sprite;
};
sf::Sprite Bullet::getSprite()
{
	return sprite;
};

void Bullet::setActive(bool isActive)
{
	this->isActive = isActive;
};
bool Bullet::getActive()
{
	return isActive;
};
