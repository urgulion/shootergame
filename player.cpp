#include "player.h"


Player::Player() : Pawn()
{
	x = 60;
	y = 600;
	direction = 0;
	
	image.loadFromFile("images/player.png");
	image.createMaskFromColor(sf::Color(255, 255, 255));
	texture.loadFromImage(image);
	sprite.setTexture(texture);
	sprite.setPosition(x, y);
	sprite.setOrigin(width / 2, height / 2);
};

void Player::move(float deltaTime, Obstacle *obstacle, int n)
{

	if (!collisionDetection(obstacle, n))
	{
		if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Up) || (sf::Keyboard::isKeyPressed(sf::Keyboard::W))))
		{
			x += std::sin(direction * 3.14159265 / 180) * deltaTime * 0.01;
			y -= std::cos(direction * 3.14159265 / 180) * deltaTime * 0.01;
		}
		if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Down) || (sf::Keyboard::isKeyPressed(sf::Keyboard::S))))
		{
			x -= std::sin(direction * 3.14159265 / 180) * deltaTime * 0.01;
			y += std::cos(direction * 3.14159265 / 180) * deltaTime * 0.01;
		}
		if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Left) || (sf::Keyboard::isKeyPressed(sf::Keyboard::A))))
		{
			direction -= 0.1;
			sprite.setRotation(direction);
		}
		if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Right) || (sf::Keyboard::isKeyPressed(sf::Keyboard::D))))
		{
			direction += 0.1;
			sprite.setRotation(direction);
		}
	}
	else
	{
		if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Up) || (sf::Keyboard::isKeyPressed(sf::Keyboard::W))))
		{
			x -= std::sin(direction * 3.14159265 / 180) * deltaTime * 0.05;
			y += std::cos(direction * 3.14159265 / 180) * deltaTime * 0.05;
		}
		if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Down) || (sf::Keyboard::isKeyPressed(sf::Keyboard::S))))
		{
			x += std::sin(direction * 3.14159265 / 180) * deltaTime * 0.05;
			y -= std::cos(direction * 3.14159265 / 180) * deltaTime * 0.05;
		}
		if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Left) || (sf::Keyboard::isKeyPressed(sf::Keyboard::A))))
		{
			direction -= deltaTime;
			sprite.setRotation(direction);
		}
		if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Right) || (sf::Keyboard::isKeyPressed(sf::Keyboard::D))))
		{
			direction += deltaTime;
			sprite.setRotation(direction);
		}
	}
	
	positionCorrection(obstacle, n);

	sprite.setPosition(x, y);

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
	{
		shoot();		
	}

}
