#include <SFML/Graphics.hpp>
#include "obstacle.h"


#ifndef BULLET_H
#define BULLET_H

class Bullet
{
	private:
		double x, y;
		float width, height, direction; 

		sf::Texture texture;
		sf::Sprite sprite;
		sf::Image image;

		bool isActive;

	public:
		Bullet();
		void setParameters(int x, int y, float direction);
		bool move(float deltaTime, Obstacle *obstacle, int n, double pawnX, double pawnY, float pawnHeight, float pawnWeight);

		void setX(int x);
		int getX();

		void setY(int y);
		int getY();

		void setWidth(float width);
		float getWidth();

		void setHeight(float height);
		float getHeight();

		void setDirection(float direction);
		float getDirection();

		void setTexture(sf::Texture texture);
		sf::Texture getTexture();

		void setSprite(sf::Sprite sprite);
		sf::Sprite getSprite();

		void setActive(bool isActive);
		bool getActive();
};

#endif